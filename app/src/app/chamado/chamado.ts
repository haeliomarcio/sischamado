export interface Chamado{
    id?: number;
    data?: Date;
    nome: string;
    email: string;
    titulo: string;
    descricao: string;
    telefone: string;
}