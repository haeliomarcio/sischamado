import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chamado } from './chamado';
import { environment } from '../../environments/environment';


@Injectable()
export class ChamadoService{

    private chamado: Chamado;
    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient){    }

    getChamados(email){
        return this.http.get<Object[]>(this.baseUrl + '/chamados/email/'+email);
    }

    addChamados(chamado: Chamado){
        return this.http.post(this.baseUrl + '/chamados', chamado);
    }
}