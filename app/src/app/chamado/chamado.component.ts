import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { ChamadoService } from './chamados.services';
import { Chamado } from './chamado';

@Component({
  selector: 'app-chamado',
  templateUrl: './chamado.component.html',
  styleUrls: ['./chamado.component.css'],
  providers: [FormBuilder]
})
export class ChamadoComponent implements OnInit {

  formChamado: FormGroup;
  chamado: Chamado;

  constructor(
    private http: HttpClient, 
    private chamadoService: ChamadoService,
    private formBuilder: FormBuilder
    ) {
      this.formChamado = this.formBuilder.group({
        nome: [''],
        email: [''],
        titulo: [''],
        descricao: [''],
        telefone: [''],
      });
     }

  ngOnInit() {
  }

  abrirChamado(){
    const chamado: Chamado = {
      nome: this.formChamado.get('nome').value,
      email: this.formChamado.get('email').value,
      titulo: this.formChamado.get('titulo').value,
      descricao: this.formChamado.get('descricao').value,
      telefone: this.formChamado.get('telefone').value,
    };

    this.chamadoService.addChamados(chamado)
    .subscribe(data => {
      Swal.fire({
        title: 'Sucesso!',
        text: 'Chamado aberto com sucesso. Entraremos em contato com você.',
        type: 'success',
      });
      this.formChamado.reset();
    });
    Swal.fire({
      title: 'Sucesso!',
      text: 'Chamado aberto com sucesso. Entraremos em contato com você.',
      type: 'success',
    });

  }
}
