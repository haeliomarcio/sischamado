import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ChamadoComponent } from './chamado/chamado.component';
import { TelefonesComponent } from './telefones/telefones.component';
import { LocalizacaoComponent } from './localizacao/localizacao.component';
import { ListaChamadosComponent } from './lista-chamados/lista-chamados.component';

const routes: Routes = [
  {component: HomeComponent, path: ''},
  {component: ChamadoComponent, path: 'chamados'},
  {component: TelefonesComponent, path: 'telefones'},
  {component: LocalizacaoComponent, path: 'localizacao'},
  {component: ListaChamadosComponent, path: 'lista-chamados'},
  //{component: UsuarioComponent, path: 'usuarios', canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
