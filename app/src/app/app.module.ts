import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ChamadoComponent } from './chamado/chamado.component';
import { LocalizacaoComponent } from './localizacao/localizacao.component';
import { ListaChamadosComponent } from './lista-chamados/lista-chamados.component';
import { TelefonesComponent } from './telefones/telefones.component';
import { ChamadoService } from './chamado/chamados.services';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ChamadoComponent,
    LocalizacaoComponent,
    ListaChamadosComponent,
    TelefonesComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    SweetAlert2Module.forRoot() 
  ],
  providers: [ChamadoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
