<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chamado;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendMailChamado;
use Illuminate\Support\Facades\Mail;

class ChamadosController extends Controller
{

    protected $chamados;

    public function __construct(Chamado $chamados)
    {
        $this->chamados = $chamados;    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $chamados = $this->chamados->where('email', $request->email)->get();
        return $chamados;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacao = [
            'nome' => 'required',
            'email' => 'required',
            'titulo' => 'required',
            'descricao' => 'required',
            'telefone' => 'required'
        ];

        $validator = Validator::make($request->all(), $validacao);

        if(!$validator->fails()){
            $chamado = $this->chamados->create($request->all());
            Mail::to('haelioferreira@yahoo.com.br')->send(new SendMailChamado($chamado));
            return response()->json(['chamado' => $chamado, 'status' => 200]);
        } else {
            return response()->json(['msg' => 'Não foi possível abrir o chamado', 'status' => 400]);
        }
    }

    /** 
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
