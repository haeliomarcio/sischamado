<?php

use Illuminate\Database\Seeder;
use App\Models\Chamado;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        Chamado::create([
            'nome' => 'Max',
            'email' => 'teste@teste.com',
            'titulo' => 'Computador QUebrado',
            'descricao' => 'Teste Teste',
            'telefone' => '8588887777',
        ]);
    }
}
