<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Mail\SendMailChamado;

Route::get('email', function(){
    $chamado = ['Teste' => 'Teste'];
    Mail::to('haelioferreira@yahoo.com.br')->send(new SendMailChamado($chamado));
});
Route::resources(['chamados' => 'ChamadosController']);